package fr.iut;

import java.text.NumberFormat;
import java.util.Locale;
import static java.lang.System.*;

/**
 * Created by Flo on 02/02/2016.
 */
public class StringUtil {
    public String prettyCurrencyPrint(final double amount, final Locale locale) {
        if( locale != null )
            return NumberFormat.getCurrencyInstance(locale).format(amount);
        else
            new NullPointerException();
        return "";
    }

}
