package fr.iut;

import java.util.Locale;
import static java.lang.System.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        out.println( "Hello World!" );
        StringUtil test = new StringUtil();
        out.println(test.prettyCurrencyPrint(3333.33, Locale.FRANCE));
    }
}
