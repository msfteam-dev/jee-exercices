package fr.iut.rm.constraints;


import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy={ValidName.RoomNameValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD,
        ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidName{
    String message() default "Incorrect name"; // Message si pas valide
    Class<?>[] groups() default {}; // Groupe de validation partiel
    Class<? extends Payload>[] payload() default{}; // Metadonnée de la contrainte


    class RoomNameValidator implements ConstraintValidator<ValidName, String> {
        @Override
        public void initialize(ValidName validName) {

        }

        public boolean isValid(String value, ConstraintValidatorContext context){
            if( value == null || value.length() == 0 ){
                return true;
            }
            if( value.length() < 3 || value.length() > 30 ){
                return false;
            }
            return true;
        }
    }
}
