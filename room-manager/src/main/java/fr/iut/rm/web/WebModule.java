package fr.iut.rm.web;

import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.ServletModule;
import fr.iut.rm.web.servlet.*;
import fr.iut.rm.web.servlet.servletScript.deleteRoomScript;
import fr.iut.rm.web.servlet.servletScript.addRoomScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fred on 08/03/15.
 */
public class WebModule extends ServletModule {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(WebModule.class);

    @Override
    protected final void configureServlets() {
        super.configureServlets();

        logger.info("WebModule configureServlets started...");
        serve("/rooms").with(ListServlet.class);

        serve("/admin/home").with(AdminServlet.class);

        serve("/admin/addRoom").with(AddRoomServlet.class);
        serve("/admin/addRoomScript").with(addRoomScript.class);

        serve("/admin/deleteRoom").with(DeleteRoomServlet.class);
        serve("/admin/deleteRoomScript").with(deleteRoomScript.class);

        serve("/enterRoom").with(EnterRoomServlet.class);

        serve("/leaveRoom").with(LeaveRoomServlet.class);




        logger.info("   install JpaPersistModule room-manager");
        install(new JpaPersistModule("room-manager"));

        logger.info("   install servlet filter");
        filter("/*").through(PersistFilter.class);

        logger.info("WebModule configureServlets ended.");
    }
}
