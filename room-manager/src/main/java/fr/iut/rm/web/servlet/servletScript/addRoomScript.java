package fr.iut.rm.web.servlet.servletScript;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet simply
 */
@Singleton
public class addRoomScript extends HttpServlet {
    /**
     * the dao used to access room persisted data
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("room_name");
        String capacity = req.getParameter("room_capacity");
        if (name != null && capacity != null) {
            Room room = new Room();
            room.setName(name);
            room.setCapacity(Integer.parseInt(capacity));
            roomDao.saveOrUpdate(room);
            /*
            // implementation des QRCodes
            QRCode codeEnter = new QRCode();
            codeEnter.init("http://localhost:8080/enterRoomScript?room_name=" + name);
            QRCode codeExit = new QRCode();
            codeExit.init("http://localhost:8080/leaveRoomScript?room_name=" + name);
            */
        }

        resp.sendRedirect("../rooms");
    }
}
