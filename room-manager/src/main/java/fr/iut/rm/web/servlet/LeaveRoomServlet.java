package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Demonstration servlet for bootstrap and freemarker usage.
 */
@Singleton
public class LeaveRoomServlet extends HttpServlet {
    /**
     * constant for UTF-8 *
     */
    private static final String TEMPLATE_ENCODING = "UTF-8";
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(BootFreeServlet.class);


    /**
     * HTTP GET access
     *
     * @param request  nothing required
     * @param response response to sent
     * @throws ServletException by container
     * @throws IOException      by container
     */

    @Inject
    RoomDao roomDao;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("room_name");
        if (name != null) {
            Room room = roomDao.findByName(name);
            if(room.getNbPersonne()>0) {
                room.setNbPersonne(room.getNbPersonne() - 1);
                roomDao.saveOrUpdate(room);
            }
        }

        response.sendRedirect("../rooms");
    }
}

