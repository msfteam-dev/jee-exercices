package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet simply
 */
@Singleton
public class Menu extends HttpServlet {
    /**
     * the dao used to access room persisted data
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        String room = req.getParameter("Rooms");
        String add = req.getParameter("Add");
        String delete = req.getParameter("Delete");
        String init = req.getParameter("Init");

        if (room != null) {
            resp.sendRedirect("../Rooms");
        }
        if (add != null) {
            resp.sendRedirect("../adminAdd");
        }
        if (delete != null) {
            resp.sendRedirect("../Admin/Home");
        }
        if (init != null) {
            resp.sendRedirect("../Init");
        }

    }
}

