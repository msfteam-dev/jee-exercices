package fr.iut.rm.persistence.domain;

import fr.iut.rm.constraints.ValidName;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * A classic room
 */
@Entity
@Table(name = "room")
public class Room {
    /**
     * sequence generated id
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * Room's name
     */
    @Column(nullable = false, unique = true)
    @ValidName
    private String name;

    /**
     * Room's capacity
     */
    @Column(nullable = false)
    @Min(1)
    private long capacity;

    /**
     * Room's nbPersonne
     */
    @Min(0)
    private long nbPersonne;

    /**
     * Default constructor (do nothing)
     */
    public Room() {
        // do nothing
    }

    /**
     * anemic getter
     *
     * @return the room's id
     */
    public long getId() {
        return id;
    }

    /**
     * anemic setter
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * anemic getter
     *
     * @return the calling number
     */
    public String getName() {
        return name;
    }

    /**
     * anemic setter
     *
     * @param name the new calling number to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * anemic getter
     *
     * @return the capacity
     */
    public long getCapacity() {
        return capacity;
    }

    /**
     * anemic setter
     *
     * @param capacity the new calling number to set
     */
    public void setCapacity(final int capacity) {
        this.capacity = capacity;
    }

    /**
     * anemic getter
     *
     * @return the number of Personne
     */
    public long getNbPersonne() {
        return nbPersonne;
    }

    /**
     * anemic setter
     *
     * @param nbPersonne the new calling number to set
     */
    public void setNbPersonne(final long nbPersonne) { this.nbPersonne = nbPersonne; }
}
