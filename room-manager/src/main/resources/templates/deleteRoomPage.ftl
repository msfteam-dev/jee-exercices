<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<h1>Liste de Salle</h1>
<#list rooms as room>
<li>
    <ul>${room.name}</ul>
</li>
</#list>
<h1>Supprimer une salle</h1>
<form method="get" action="deleteRoomScript">
    <input type="text" name="room_name">
    <input type="submit" value="Envoyer">
</form>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>

</script>
</body>
</html>