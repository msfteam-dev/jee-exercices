<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<h1>Menu</h1>
<p><a href="admin/addRoom">Add a room</a></p>
<p><a href="admin/deleteRoom">Delete a room</a></p>
<p>
<h1>Liste de Salle</h1>

<#list rooms as room>
    <table border="2">
        <tr>
            <th>Room name</th>
            <th>Number of person</th>
            <th>Capacity of person</th>
        </tr>
        <tr>
            <td>${room.name}</td>
            <td>${room.nbPersonne}</td>
            <td>${room.capacity}</td>
        </tr>
    </table>
</#list>

</p>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>

</script>
</body>
</html>