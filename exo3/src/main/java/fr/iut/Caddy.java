package fr.iut;

import fr.iut.club.Club;
import com.google.inject.Inject;
import com.google.inject.name.Named;
/**
 * A caddy has several clubs and know which club to use depending on conditions
 */
public class Caddy {
    @Inject
    @Named("Putter")
    private Club putter;
    @Inject
    @Named("Wood")
    private Club wood;
    /**
     * default empty constructor
     */
    public Caddy() {
    }
    public Club getClub(final Condition conditions) {
        switch (conditions) {
            case GREEN:
                return putter;
            case FAIRWAY:
                return wood;
            default:
                return putter;
        }
    }
}