package fr.iut.club;

import fr.iut.Ball;

public interface Club {
    void shootBall(final double strength, final double direction, Ball ball);
}
