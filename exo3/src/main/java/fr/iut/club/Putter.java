package fr.iut.club;

import fr.iut.Ball;

import java.awt.geom.Point2D;

public class Putter implements Club {
    public void shootBall(double strength, double direction, Ball ball) {
        double x = ball.getPosition().getX();
        double y = ball.getPosition().getY();
        x += (strength * 100) * Math.sin(direction);
        y += (strength * 100) * Math.cos(direction);
        ball.setPosition(new Point2D.Double(x, y));
    }
}
