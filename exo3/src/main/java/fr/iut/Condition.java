package fr.iut;

/**
 * Created by Flo on 09/02/2016.
 */
public enum Condition {
    GREEN,
    FAIRWAY,
    BUNKER
}
