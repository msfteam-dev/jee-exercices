package fr.iut;

import java.awt.geom.Point2D;

/**
 * Created by Flo on 09/02/2016.
 */
public class Ball {
    private final Point2D position = new Point2D.Double(0,0);

    public Ball(){
        this.position.setLocation(0,0);
    }

    public Point2D getPosition() {
        return position;
    }

    public void setPosition(final Point2D pos){
        this.position.setLocation(pos);
    }
}
